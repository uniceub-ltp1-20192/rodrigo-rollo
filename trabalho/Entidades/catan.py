from Entidades.jogos import Boardgame


class Catan(Boardgame):
    def __init__(self, tempoJogo ="longo"):
        super().__init__()
        self._tempoJogo = tempoJogo

    @property
    def tempoJogo(self):
        return self._tempoJogo

    @tempoJogo.setter
    def tempoJogo(self, tempoJogo):
        self._tempoJogo = tempoJogo

    def comprar(self):
        print("fazendo compra")

    def __str__(self):
        return '''
    ---- Catan ---- 
    Identificador: {}
    numeroJogadores: {}
   nome : {}
    preco : {}
    Tempo de jogo: {}
    '''.format(self.identificador, self. numeroJogadores
               , self.nome, self.preco, self.tempoJogo)