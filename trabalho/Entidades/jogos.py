class Boardgame:

    def __init__(self, identificador=0, numeroJogadores = 0, nome = "", preco = 0):
        self._identificador = identificador
        self._numeroJogadores = numeroJogadores
        self._nome = nome
        self._preco = preco

    @property
    def identificador(self):
        return self._identificador

    @identificador.setter
    def identificador(self, identificador):
        self._identificador = identificador

    @property
    def numeroJogadores(self):
        return self._numeroJogadores

    @numeroJogadores.setter
    def numeroJogadores(self, numeroJogadores):
        self._numeroJogadores = numeroJogadores

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, nome):
        self._nome = nome


    @property
    def preco(self):
        return self._preco

    @preco.setter
    def preco(self, preco):
        self._precoo = preco

    def comprar(self):
        print("Comprado")

    def jogar(self):
        print("Sendo jogado")