from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.catan import Catan


class Menu:

    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")

    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("entrou em Consultar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if (retorno != None):
                            print(retorno)
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)

                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if (retorno != None):
                            print(retorno)
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse atributo
                            """)

                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""

            elif opMenu == "2":
                print("entrou em Inserir")
                Menu.menuInserir(d)

            elif opMenu == "3":
                print("entrou em alterar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno, d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno, d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("entrou em deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno, d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno, d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opcao valida")

    @staticmethod
    def menuBuscaPorIdentificador(d: Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d: Dados):
        retorno = d.buscarPorAtributo(input("Informe um tamanho para busca:"))
        return retorno

    @staticmethod
    def menuInserir(d):
        p = Catan()
        p.numeroJogadores = input("Informe o numero de jogadores:")
        p.preco = input("Informe o preço:")
        p.nome = input("Informe a nome:")
        p.tempoJogo = input("Informe o tempo de jogo:")
        d.inserir(p)

    @staticmethod
    def menuAlterar(retorno, d):
        retorno.preco = Validador.validarValorInformado(retorno.preco, "Informe um preço:")
        retorno.numeroJogadores = Validador.validarValorInformado(retorno.numeroJogadores, "Informe o numero de jogadores:")
        retorno.nome = Validador.validarValorInformado(retorno.nome, "Informe o nome:")
        retorno.tempoJogo = Validador.validarValorInformado(retorno.tempoJogo, "Informe o tempo de jogo:")
        d.alterar(retorno)

    def menuDeletar(retorno, d):
        print(retorno)
        deletar = input("""
        Deseja deletar o registro:
        S - Sim
        """)
        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("""
            Registro deletado
            """)
        else:
            print("""
            Registro nao foi deletado
            """)

