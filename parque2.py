def askName():
    name = input("Digite seu nome: ")
    return name

def validateString(name):
    return type(name) == str

def askHeigth():
    test2 = float(input("Digite sua altura: "))

    return test2

def validateFloat(heigth):
    return type(heigth) == float

def askNumberFriends():
    test3 = int(input("Digite com quantos amigos você esta: "))

    return test3

def validateInt(x):
    return type(x) == int

def validateName(name):
    return name.count(" ") >= 1

def validateHeigth(heigth):
    return heigth > 1.61 or heigth == 1.51

def validateFriends(friends):
    return friends != 0

def checkPermission(name, heigth, friends):
    return (name.count(" ") >= 1 and heigth > 1.61 and friends > 0) or (heigth == 1.51 and friends == 4)
def checkPrize(cabinNumber, userCount):
    if cabinNumber == 10 and userCount == 100:
        return ("Voce ganhou o premio")

cabinNumber = 1
userCount = 0

while True:
    # solicita nome para o usuário


    name = askName()

    # solicita altura para o usuário
    heigth  = askHeigth()

    # solicita quantidade de acompanhantes para o usuário
    friends = askNumberFriends()

    # verifica se o valor armazenado na variável name é uma string
    # e armazena o resultado em vs
    vs    = validateString(name)

    # verifica se o valor armazenado na variável heigth é um float
    # e armazena o resultado em vf
    vf    = validateFloat(heigth)

    # verifica se o valor armazenado na variável friends é um inteiro
    # e armazena o resultado em vi
    vi    = validateInt(friends)

    # checa validade das entradas
    if (not(vs and vf and vi)):
        print("Não pode!")
        break

    # verifica se as entradas correspondem a realidade
    # do cenário apresentado
    vname    = validateName(name)
    vheigth  = validateHeigth(heigth)
    vfriends = validateFriends(friends)

    # chaca validade da realidade
    if (not(vname and vheigth and vfriends)):
        print("Tá Errraado!")
        break

    # verifica se o usuário pode utilizar a roda gigante
    allowed = checkPermission(name, heigth, friends)


    if not allowed:
        print("Bye bye!")
        break
    print("Voce pode entrar", cabinNumber)

    cabinNumber = cabinNumber + 1
    userCount = userCount + 1
    if cabinNumber >10:
        cabinNumber = cabinNumber - 10


    # verifica se usuário ganhou o prêmio
    uhuu = checkPrize(cabinNumber, userCount)
